from string import Template

t_xml = Template("""\
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<Experiment_plan>
$simulations
</Experiment_plan>\
""")

t_simulations = Template("""\
<Simulation id="$id" sourcePath="$gamlfile" finalStep="$finalstep" experiment="$experiment">
<Parameters>
$parameters
</Parameters>
<Outputs>
$outputs
</Outputs>
</Simulation>""")

def parameters(params):
  '''
  return the representation of parameters in xml
  args:
    params :: List Dictionary (see autogen.py)
  '''
  # template of <Parameter />
  t = Template('<Parameter name="$description" variable="$variable" type="$datatype" value="$value" />')
  #
  # substitution callback
  # maybe the datatype must be uppercase
  #
  def xml_parameter(param):
    subs = param.copy()
    subs['datatype'] = subs['datatype'].upper()
    return t.substitute(subs)
  #
  # map the callback to each parameters
  # return the joined xml string
  # 
  xml_parameters = map(xml_parameter, params)
  return ("\n").join(xml_parameters)

def outputs(outputs):
  '''
  return the representation of outputs in xml
  args:
    outputs :: List String (see autogen.py)
  '''
  xmloutputs = []
  t = Template('<Output id="$id" name="$output" framerate="1"/>')
  # 
  # pad 0 to the left, pad left is justify right
  #
  njust = len(str(len(outputs))) - 1
  for (idx, output) in enumerate(outputs):
    subs = {}
    subs['id'] = str(idx).rjust(njust, '0')
    subs['output'] = output
    xmloutputs.append(t.substitute(subs))
  return ("\n").join(xmloutputs)

def simulation(gamlfile, idx, experiment, finalstep = 2880):
  """
    simulation(gamlfile, idx, experiment)
  take filename + idx + single experiment and generate simutation xml
  """
  subs = experiment.copy()
  subs['gamlfile'] = gamlfile
  subs['finalstep'] = finalstep
  subs['parameters'] = parameters(experiment['parameters'])
  subs['outputs'] = outputs(experiment['outputs'])
  subs['id'] = idx
  subs['experiment'] = experiment['name']
  return t_simulations.substitute(subs)

def xml(gamlfile, experiments, finalstep = 2880):
  npad = len(str(len(experiments))) - 1
  subs = {'simulations': ''}
  for (idx, e) in enumerate(experiments):
    idx = str(idx).rjust(npad, '0')
    s = simulation(gamlfile, idx, e, finalstep)
    subs['simulations'] = subs['simulations'] + "\n" + s
  return t_xml.substitute(subs)

