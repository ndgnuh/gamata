from string import Template
import hashlib
import os
import sys
sys.path.append(os.path.dirname(__file__))
import lex
import parse
import template
from pprint import pprint
import config
pprint(sys.path)

def process_gaml(gamlfile):
  lines = lex.extract_lines(gamlfile)
  variables = lex.extract_variables(lines)
  variables = parse.parse_variables(variables)
  experiments = lex.extract_experiment(lines)
  experiments = parse.parse_experiments(variables, experiments)
  return experiments

def prompt_text(param):
  '''
  generate prompt text based on parameter
  '''
  subs = param.copy()
  #
  # the label is the description
  #
  t = '[$datatype] $description'
  #
  # if there's a list of possible value
  # then include it in the prompt
  #
  if 'among' in param.keys():
    subs['among'] = "/".join(subs['among'])
    t = '%s ($among)' % t
  #
  # if there's min or max bound then
  # include it in the prompt
  #
  if 'min' in param.keys():
    t = "%s (> $min)" % t
  if 'max' in param.keys():
    t = "%s (< $max)" % t
  t = '%s (default: $default)? ' % t
  t = Template(t)
  return t.substitute(subs)

def prompt(param):
  '''
  prompt inputing the value of the parameters
  '''
  newparam = param.copy()
  while True:
    satisfied = True
    value = input(prompt_text(param))
    #
    # if the input is empty, use the default value
    #
    if value == "":
      value = param['default']
    #
    # check for conditions, min and max are not implemented
    # in the parser yet, the datatype can be mapped directly
    # to python in a lowercase function
    #
    if param['datatype'].lower() != "string":
      dtype = eval(param['datatype'].lower())
    else:
      dtype = str
    #
    # try if the value is valid
    #
    try:
      dtype(value)
    except ValueError:
      satisfied = False
    #
    # bounding condition
    #
    if 'among' in param.keys():
      satisfied = satisfied and value in param['among']
    if 'min' in param.keys():
      satisfied = satisfied and dtype(value) >= dtype(param['min'])
    if 'max' in param.keys():
      satisfied = satisfied and dtype(value) <= dtype(param['max'])
    #
    # let the user go if value satisfied
    #
    if satisfied:
      break
  newparam['value'] = value
  return newparam

def process_experiment(e):
  e = e.copy()
  nparams = len(e['parameters'])
  for i in range(nparams):
    param = e['parameters'][i]
    e['parameters'][i] = prompt(param)
  return e

def xmlname(gamlfile):
  xmlfile = gamlfile.lower()
  xmlfile = os.path.basename(xmlfile)
  xmldir = os.path.dirname(gamlfile)
  xmlfile = xmlfile.replace(".gaml", ".xml")
  xmlfile = os.path.join(xmldir, xmlfile)
  return xmlfile

def main():
  '''
  prompting to input a list of 
  '''
  inputdir = config.get_inputdir()
  outputdir = config.get_outputdir()
  if not os.path.isdir(inputdir):
    print("inputs NOT found")
    return []
  else:
    for f in os.listdir(inputdir):
      if f.lower().endswith(".gaml"):
        gamlfile = os.path.join(inputdir, f)
        print("[FILE]", gamlfile)
        #
        # Ask for the simulation name first
        # since some people might want to skip
        # everything half way
        #
        xmlfile = input("Name this simulation (optional)? ")
        experiments = process_gaml(gamlfile)
        #
        # process_experiment prompt the users for inputs
        #
        experiments = map(process_experiment, experiments)
        experiments = list(experiments)
        print("[INFO]", len(experiments), "experiment(s) found")
        xml = template.xml(f, experiments)
        #
        # ask for file name and write the xml to the
        #
        if len(xmlfile) == 0:
          xmlfile = hashlib.md5(xmlfile.encode('utf-8')).hexdigest()
        xmlfile = os.path.join(inputdir, "%s.xml" % xmlfile)
        f = open(xmlfile, "w")
        f.write(xml)
        f.close()
    return 0

if __name__ == "__main__":
  main()
  input("Press enter")
