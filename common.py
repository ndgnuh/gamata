outputsdir = "outputs"
inputsdir = "inputs"
csvsdir = "csv"

def fbasename(energy_miss, scale, nb_aphids_init, init_position):
   return ("{:1.4f}-{:04d}-{:04d}-{:s}").format(
         energy_miss
         , scale
         , nb_aphids_init
         , init_position
         )
