from os import path
import pprint
import argparse
import os
import functools

default = {
		"inputdir": "inputs",
		"outputdir": "outputs",
		"loglevel": 3,
		}

def gama_from_gamahome(gama_home):
	gama_headless = os.path.join(gama_home, "headless", "gama-headless")
	if os.name == "nt":
		return gama_headless + ".bat"
	else:
		return gama_headless + ".sh"

def main():
	parser = argparse.ArgumentParser(description='Config for gamata')
	parser.add_argument(
			'--loglevel',
			dest='loglevel',
			default=default['loglevel'],
			type=int,
			metavar="level",
			required=False,
			help="Log level"
			)
	parser.add_argument(
			'--outputdir', '-o',
			dest="outputdir",
			default=default["outputdir"],
			type=str,
			metavar="PATH",
			required=False,
			help="output directory")
	parser.add_argument(
			'--inputdir', '-i',
			dest="inputdir",
			default=default["inputdir"],
			type=str,
			metavar="PATH",
			required=False,
			help="input dir")
	parser.add_argument(
			'--gama-home',
			dest="gama_home",
			default=None,
			type=str,
			metavar="PATH",
			required=False,
			help="Path to GAMA install directory")
	parser.add_argument(
			'--gama-headless',
			dest="gama_headless",
			default=None,
			type=str,
			metavar="PATH",
			required=False,
			help="Path to GAMA headless")
	parser.add_argument(
			'-r', '--rep', '--replication',
			dest="replication",
			default=None,
			type=int,
			metavar="N",
			required=False,
			help="Number of replication")
	args = parser.parse_args()
	args = vars(args)
	if args.get("gama_home") is not None:
		args["gama_headless"] = gama_from_gamahome(args["gama_home"])
	try:
		with open("config.json", "r") as fd:
			cfg = json.load(fd)
			for k in cfg.keys():
				if args.get(k) is None:
					args[k] = cfg[k]
	except FileNotFoundError as e:
		print("Creating a new config")
	except ValueError:
		print("Corrupted config, try remove config.json")
	with open("config.json", "w") as fd:
		json.dump(args, fd)

import json

__cfg = {}

def set(key, val):
	__cfg[key] = val
	save()

def keys():
	try:
		fd = open("config.json", "r")
		cfg = json.load(fd)
		fd.close()
	except FileNotFoundError as e:
		print("config doesn't exist")
	except ValueError:
		print("corrupted config")
	return cfg.keys()

def get(key = None):
	try:
		fd = open("config.json", "r")
		cfg = json.load(fd)
		fd.close()
	except FileNotFoundError as e:
		print("config doesn't exist")
	if key is None:
		return cfg
	else:
		return cfg.get(key) or default.get(key)

def set(key, value):
	try:
		fd = open("config.json", "r")
		cfg = json.load(fd)
		fd.close()
		cfg[key] = value
		with open("config.json", "w") as fd:
			json.dump(cfg, fd)
		return cfg
	except FileNotFoundError as e:
		print("config doesn't exist, creating")
		fd = open("config.json", "w")
		json.dump({}, fd)
		fd.close()
		return set(key, value)
	except ValueError:
		print("corrupted config")

get_replication = functools.partial(get, "replication")
get_gama_home = functools.partial(get, "gama_home")
get_gama_headless = functools.partial(get, "gama_headless")
get_inputdir = functools.partial(get, "inputdir")
get_outputdir = functools.partial(get, "outputdir")
get_loglevel = functools.partial(get, "loglevel")
set_replication = functools.partial(set, "replication")
set_gama_home = functools.partial(set, "gama_home")
set_gama_headless = functools.partial(set, "gama_headless")
set_inputdir = functools.partial(set, "inputdir")
set_outputdir = functools.partial(set, "outputdir")
set_loglevel = functools.partial(set, "loglevel")

if __name__ == "__main__":
	main()
