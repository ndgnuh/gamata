import os
from common import fbasename, outputsdir, inputsdir
from string import Template
import sys
import re

def typecheck(a):
   atype = None
   a = a.strip()
   if re.match("^\d+$", a) is not None:
      atype = "INT"
   elif re.match("^\d+.\d+$", a) is not None:
      atype = "FLOAT"
   else:
      atype = "STRING"
   return atype


def main():
   print("LƯU Ý: Để trống == mặc định")
   os.makedirs(inputsdir, exist_ok=True)

   args = {
         "nb_aphids_init": "INT"
         , "aphid_max_energy": "FLOAT"
         , "aphid_max_transfer": "FLOAT"
         , "aphid_energy_consum": "FLOAT"
         , "aphid_proba_reproduce": "FLOAT"
         , "aphid_nb_max_offsprings": "INT"
         , "aphid_energy_reproduce": "FLOAT"
         , "aphid_energy_move": "FLOAT"
         , "scale": "INT"
         , "energy_miss": "FLOAT"
         , "init_position": "STRING"
         }

   vietnamese = {
         "nb_aphids_init": "Lượng giày nâu khởi tạo"
         , "aphid_max_energy": "Năng lượng tối đa của giày nâu"
         , "aphid_max_transfer": "Năng lượng chuyển hóa tối đa từ lúa -> giày"
         , "aphid_energy_consum": "Năng lượng tiêu thụ của giày mỗi bước thời gian"
         , "aphid_proba_reproduce": "Xác suất đẻ trứng"
         , "aphid_nb_max_offsprings": "Lượng trứng đẻ"
         , "aphid_energy_reproduce": "Ngưỡng năng lượng đẻ"
         , "aphid_energy_move": "Ngưỡng di chuyển"
         , "scale": "Khoảng cách từ lề trái tới hoa"
         , "energy_miss": "Năng lượng mất khi đi vào hoa"
         , "init_position": """Phân bố khởi tạo giày nâu
Giá trị có thể nhận:
- nn: ngẫu nhiên
- goc: ở góc bản đồ
- bien: ở biên của bản đồ
- tam: ở chính giữa bản đồ
"""
}

   defaults = {
         "nb_aphids_init": 20
         , "aphid_max_energy": 1.0
         , "aphid_max_transfer": 0.1
         , "aphid_energy_consum": 0.025
         , "aphid_proba_reproduce": 0.07
         , "aphid_nb_max_offsprings": 12
         , "aphid_energy_reproduce": 0.8
         , "aphid_energy_move": 0.2
         , "scale": 45
         , "energy_miss": 0.025
         , "init_position": "nn"
         }
   values = {}
   for arg in args:
      print()
      value = input(vietnamese[arg] + "\nInput: ")
      if value == "":
         value = str(defaults[arg])
         print("Đầu vào trống ==> dùng giá trị mặc định")
      valtype = typecheck(value)
      while valtype != args[arg]:
         print("""
         Kiểu dữ liệu không hợp lệ, yêu cầu kiểu %s. Ví dụ:
         - INT: 121493
         - FLOAT: 2.41 (phải có dấu chấm)
         - STRING: goc
         """)
         value = input(vietnamese[arg] + "\nInput: ")
      if valtype == "INT":
         value = int(value)
      elif valtype == "FLOAT":
         value = float(value)
      if arg == "init_position":
         while value not in ["nn", "goc", "bien", "tam"]:
            value = input(vietnamese[arg] + "\nInput: ")
      # finalize
      values[arg] = value

   cfg = list(values.keys())
   cfg = map(lambda k: "%s = %s" % (k, values[k]), cfg)
   cfg = ("\n").join(cfg)
   # [energy_miss]-[scale]-[lượng khởi tạo]-[loại phân bố]
   print(values)
   cfgfile = fbasename(
         values["energy_miss"]
         , values["scale"]
         , values["nb_aphids_init"]
         , values["init_position"]
         ) + ".conf"
   io = open(os.path.join("inputs", cfgfile), "w")
   io.write(cfg)
   io.close()
   input("File config: " + cfgfile + ". Press any key to continue...")

if __name__ == "__main__":
   main()

