__template = """model mophong10timelikepaddycothelaylan

global {
  float energy_miss <-0.025; //nang luong mat khi gap hoa
  float prob_move <- 0.99;
  int nb_aphids_init <- 20; // so ray khoi tao
  float aphid_max_energy <- 1.0;
  float aphid_max_transfer <- 0.1;
  float aphid_energy_consum <- 0.025;
  float egg_proba_die <- 0.0025;
  float aphid_proba_die <- 0.04;
  float aphid_proba_reproduce <- 0.07;
  int age_init <-  168;         // tuoi de co the di chuyen
  float age_reproduce <- 504;
  float aphid_energy_reproduce <- 0.8;
  int aphid_nb_max_offsprings <- 12;
  int aphid_nb_min_offsprings <- 5;
  float aphid_energy_move <- 0.2; //nang luong thap nhat de di chuyen
  string  file_name <-"result1.csv";
  int nb_aphid1j-> length(aphid where ((each.age<age_init) and  (each.my_cell.grid_x<scale_y/2)));
  int nb_aphid2j -> length(aphid where  ((each.age<age_init) and (each.my_cell.grid_x>=scale_y/2)));
  int nb_aphid1a -> length(aphid where (each.age>=age_init and  each.my_cell.grid_x<scale_y/2));
  int nb_aphid2a -> length(aphid where (each.age>=age_init and  each.my_cell.grid_x>=scale_y/2));
  int nb_paddy1 -> length (vegetation_cell where (each.food>0 and each.grid_x<scale_y/2));
  int nb_paddy2 -> length (vegetation_cell where (each.food>0 and each.grid_x>=scale_y/2));
  int scale <- 45;
  int scale_y <- 100;


  init {

    create aphid number: nb_aphids_init{} 
    ask vegetation_cell {

      if grid_x<scale or grid_x >=(scale_y-scale){
        food <-0.4;
        color <- rgb(int(255 * (1 - food)), 255, int(255 * (1 - food)));
      }
      else{
        food<-0;
        //color <- rgb(255, 165, 0);
        color <- rgb(238, 162, 173);

      }
      food_prod <- 0.005 ; 
    }}

  reflex aff {
    write "message at #" + cycle + "/2880";
  }

  // reflex write_to_file {
  //   save [nb_aphid1j, nb_aphid2j, nb_aphid1a, nb_aphid2a,  nb_paddy1,  nb_paddy2 ] to: file_name rewrite: (cycle = 0) ? true : false type:"csv";
  // }
  reflex stop_simulation when:  (cycle=2880)  {
    do pause ;
  }}

species aphid {
    float size <- 1.0 ;

    rgb color;
    int aphids_number_reproduce <- 0;
    float max_energy <- aphid_max_energy ;
    float max_transfer <- aphid_max_transfer ;
    float energy_consum <- aphid_energy_consum ;   
    float proba_reproduce <- aphid_proba_reproduce;
    int nb_max_offsprings <- aphid_nb_max_offsprings;
    float energy_reproduce <- aphid_energy_reproduce;
    float energy_move <- aphid_energy_move;
    
   $init_position
    float energy <- rnd(0.4,0.6)  max: max_energy ;
        int age<- rnd(0,299) update: age +1;
    init { 
        location <- my_cell.location;  
    } 
    reflex update_energy when: age>=age_init {
    	 energy <- energy - energy_consum;
    }    
    reflex miss_nl when:my_cell.grid_x >=scale and my_cell.grid_x<scale_y-scale {
    	energy <- energy - energy_miss;
    }
    reflex basic_move 
   when: energy >= energy_move and age >=age_init and flip(prob_move) {
    	my_cell <- one_of (my_cell.neighbors2) ;
    	location <- my_cell.location ;}
    	//   when: energy >= energy_move and age >=age_init {
 //  list<vegetation_cell> like_paddy <- my_cell.neighbors2 where (each.food >0)   ;
  // if !empty( like_paddy){
  //  my_cell <- one_of (like_paddy) 
   // location <- my_cell.location }
  //  else {
    //	my_cell <- one_of (my_cell.neighbors2) 
    	//location <- my_cell.location }
    

    reflex change_color{
		//color <- (age <age_init and check=true) ? #blue : #red; 
		color <- (age <age_init) ? #blue : #red;
    }
    

    reflex eat when: my_cell.food > 0 and age >=age_init{ 
    float energy_transfer <- min([max_transfer, my_cell.food]) ;
    my_cell.food <- my_cell.food - energy_transfer ;
    energy <- energy + energy_transfer ;
    }
    reflex die when: energy <= 0 or age >720 or (flip(aphid_proba_die) and age >=600 and age<=720) or (age <age_init and flip(egg_proba_die)){
    do die ;
    }
    reflex reproduce when: (energy >= energy_reproduce) and (age >= age_reproduce) and (aphids_number_reproduce<21) and (flip(proba_reproduce)) {
    	aphids_number_reproduce <- aphids_number_reproduce +1;
        int nb_offsprings <- rnd(1, nb_max_offsprings);
        create species(self) number: nb_offsprings {
            my_cell <- myself.my_cell;
            location <- my_cell.location;
            energy <- 0.4;
            age <- 0;
        }
        energy <- energy - 0.1;
    }

    aspect base {
    draw circle(size) color: color ;
    }}
grid vegetation_cell width: 100 height: 100 neighbors: 4 {
    float max_food <- 1.0 ;
    float food_prod  ;
    float food  max: max_food   ;
    rgb color;
 
   list<vegetation_cell> neighbors2  <- (self neighbors_at 2);  
      reflex update_color{
      	if grid_x<scale or grid_x >=(scale_y-scale){
    	color <- rgb(int(255 * (1 - food)), 255, int(255 * (1 - food)))  ;}
    	else{
    		//color <- rgb(255, 165, 0);
    	//	color <- rgb(238, 162, 173);
    	}
    } 
	reflex update_food when: food>0  {
		food<- food +food*0.008;
	
    }}
experiment aphid_paddy type: gui {
    init {

   }
    parameter "Initial number of aphids: " var: nb_aphids_init min: 1 max: 10000 category: "aphid" ;
    parameter "aphid max energy: " var: aphid_max_energy category: "aphid" ;
    parameter "aphid max transfer: " var: aphid_max_transfer  category: "aphid" ;
    parameter "aphid energy consumption: " var: aphid_energy_consum  category: "aphid" ;
    parameter 'aphid probability reproduce: ' var: aphid_proba_reproduce category: 'aphid';
    parameter 'aphid nb max offsprings: ' var: aphid_nb_max_offsprings category: 'aphid';
    parameter 'aphid energy reproduce: ' var: aphid_energy_reproduce category: 'aphid'; 
    parameter 'aphid energy move: ' var: aphid_energy_move category: 'aphid';
    parameter 'area scale: ' var: scale category: 'aphid';
    parameter 'energy miss: ' var: energy_miss category: 'aphid';
    output {
    display main_display {
        grid vegetation_cell lines: #black ;
        species aphid aspect: base ;
    }
    		display Population_information refresh_every: 5 {
			chart "Species evolution" type: series size: {1,0.5} position: {0, 0} {
				data "number_of_aphid1a" value: nb_aphid1a color: #blue ;
				data "number_of_aphid2a" value: nb_aphid2a color: #lightblue;
				data "number_of_paddy1" value: (nb_paddy1) color: #green ;
				data "number_of_paddy2" value: (nb_paddy2) color: #lightseagreen;
				data "number_of_aphid1j" value: (nb_aphid1j) color: #red ;
				data "number_of_aphid2j" value: (nb_aphid2j) color: #yellow ;

			}}
    monitor "Number of aphid1j" value: nb_aphid1j ;
    monitor "Number of aphid2j" value: nb_aphid2j ;
    monitor "Number of aphid1a" value: nb_aphid1a;
    monitor "Number of aphid2a" value: nb_aphid2a;
    monitor "Number of paddy1" value: nb_paddy1 ;
    monitor "Number of paddy2" value: nb_paddy2 ;
    }}
"""

from string import Template as __Template
__template = __Template(__template)

substitute = __template.substitute
safe_substitute = __template.safe_substitute
