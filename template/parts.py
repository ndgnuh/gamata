init_position = {
      "nn": "vegetation_cell my_cell <- one_of(vegetation_cell where (each.grid_x <scale));"
      , "goc": "vegetation_cell my_cell <- one_of(vegetation_cell where (each.grid_x <5 and each.grid_y < 5));"
      , "bien": "vegetation_cell my_cell <- one_of(vegetation_cell where (each.grid_x <2));"
      , "tam": "vegetation_cell my_cell <- one_of(vegetation_cell where (each.grid_x < int(scale/2 )+5 and each.grid_y < int(scale_y/2 )+5 and each.grid_x > int(scale/2 )-5 and each.grid_y > int(scale_y/2 )-5));"
      }
