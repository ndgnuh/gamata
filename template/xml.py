from string import Template as __Template
__xml = """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<Experiment_plan>
  <Simulation id="1" sourcePath="$sourcefile" finalStep="2880" experiment="aphid_paddy">
    <Parameters>
      <Parameter name="Initial number of aphids: " type="INT" value="$nb_aphids_init" />
      <Parameter name="aphid nb max offsprings: " type="INT" value="$aphid_nb_max_offsprings" />
      <Parameter name="aphid max energy: " type="FLOAT" value="$aphid_max_energy" />
      <Parameter name="aphid max transfer: " type="FLOAT" value="$aphid_max_transfer"  />
      <Parameter name="aphid energy consumption: " type="FLOAT" value="$aphid_energy_consum"  />
      <Parameter name="aphid probability reproduce: " type="FLOAT" value="$aphid_proba_reproduce" />
      <Parameter name="aphid energy reproduce: " type="FLOAT" value="$aphid_energy_reproduce" />
      <Parameter name="aphid energy move: " type="FLOAT" value="$aphid_energy_move" />
      <Parameter name="area scale: " type="INT" value="$scale" />
      <Parameter name="energy miss: " type="FLOAT" value="$energy_miss" />
    </Parameters>
    <Outputs>
      <Output id="1" name="Number of aphid1j" framerate="1" />
      <Output id="2" name="Number of aphid2j" framerate="1" />
      <Output id="3" name="Number of aphid1a" framerate="1" />
      <Output id="4" name="Number of aphid2a" framerate="1" />
      <Output id="5" name="Number of paddy1" framerate="1" />
      <Output id="6" name="Number of paddy2" framerate="1" />
      <Output id="7" name="main_display" framerate="1" />
    </Outputs>
  </Simulation>
</Experiment_plan>
"""

__xml = __Template(__xml)

substitute = __xml.substitute
safe_substitute = __xml.safe_substitute
