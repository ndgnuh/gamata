import config
import functools

prefix = ["ERROR",
		"INFO",
		"WARN",
		"VERBOSE",
		"DEBUG",
		]


def lwrite(level, msg):
	loglevel = config.get_loglevel()
	if level > loglevel or level > len(prefix) or level < 0:
		return
	if type(msg) == type([]):
		msg = (" ").join(msg)
	print("[%s] %s" % (prefix[level], msg))

error = functools.partial(lwrite, 0)
info = functools.partial(lwrite, 1)
warn = functools.partial(lwrite, 2)
verbose = functools.partial(lwrite, 3)
debug = functools.partial(lwrite, 4)
