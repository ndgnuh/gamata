#!/bin/env python
from string import Template
import re

def extract_lines(f):
  """
  Read `lines` from files, strip white space and return `lines`
  ---
  lines :: List String
  """
  lines = []
  io = open(f)
  rawlines = io.readlines()
  io.close()
  #
  # `in_comments` indicates if a 
  #
  in_comments = False
  for line in rawlines:
    # if in side a multiline comments then just
    # find the closing
    # - if a closing is found, removing everything before it
    # - if a closing isn't found, ignore the line
    # else if not in side a multiline comments
    # - replace the inline comments
    # finally, strip the white space
    if in_comments:
      # look for comment closing
      closing = re.seach(r"\*\/", s)
      if not closing:
        continue
      else:
        in_comments = True
        # replace everything before the comments closing
        line = re.sub(r".*\*\/", "", line)
        pass
    else:
      # replace inline comments
      line = re.sub(r" *\/\/.*", "", line)
    # final process
    if not in_comments:
      line = line.strip()
      lines.append(line)
  return lines
    
  def remove_white_space(line):
    return line.strip()
  def remove_comments(line):
    line = re
    return line
  lines = list(map(lambda x: x.strip(), lines))
  return lines

def extract_outputs(lines):
  #
  # search for outputs in gaml files
  #
  outputs = []
  for line in lines:
    if line.startswith("monitor"):
      output_type = "monitor"
    elif line.startswith("display"):
      output_type = "display"
    else:
      continue
    outputs.append(line)
  return outputs

def extract_variables(lines):
  """
    extract_variables(lines)
  Return list of variables definition lines
  """
  variables = []
  for line in lines:
    # will broken if there is a string with ";"
    # might never happens though
    # match each line with assignment syntax
    match = re.search("(float|int|string|bool+) +([^ ]+) *<- *([^;]+)", line)
    if match:
      variables.append(line)
  return variables

def extract_experiment(lines):
  """
  Extract experiments from gaml files
  ---
  experiments :: List List String
  experiment  :: List String
  experiment  :: experiment lines in gaml file
  """
  #
  # one-pass search for experiments
  # each entry of gaml_experiments has the same structure
  # as the lines (each entry is a line, stripped)
  #
  experiments = []
  in_experiment = False
  block_level = 0
  for line in lines:
    #
    # mark the experiment block
    #
    if line.startswith("experiment"):
      experiment = []
      in_experiment = True
    #
    # if in the experiment block then append the next line
    # if there's a brace, increase block level
    # if there's a closing brace, decrease block level
    # if the block level is zero then get out of
    # the experiment block
    #
    if in_experiment:
      # append experiment
      experiment.append(line)
      # increase block level
      if re.search("{", line) is not None:
        block_level = block_level + 1
      # decrease block level
      if re.search("}", line) is not None:
        block_level = block_level - 1
      # quit the experiment block
      if block_level == 0:
        in_experiment = False
        experiments.append(experiment)
  return experiments

def extract_parameters(experiment):
  """
  Return lines with parameter definition
  experiment :: List String
  return :: List String
  """
  def is_parameter_line(line):
    return line.startswith("parameter")
  params = filter(is_parameter_line, experiment)
  return list(params)


def parse_file(f):
  io = open(f)
  lines = io.readlines()
  io.close()
  return parse(lines)

def parse(lines):
  """
  receive lines of gaml file, return the parameters
  and the outputs

  parameters: List Dictionary
  parameter: {
    description: self explanatory
    variable: gaml variable the bound to this parameter
    datatype: self explanatory
    default: the default value of the variable
    among: the set of values can be received by this variable
  }

  outputs: List String
  output: name of the output, it doesn't matter if it is
    a monitor or a display
  """
  experiments = extract_experiment(lines)
  params = []
  outputs = []
  variables = {}
  # of the line
  #
  lines = list(map(lambda x: x.strip(), lines))
  #
  # one-pass search for experiments
  # each entry of gaml_experiments has the same structure
  # as the lines (each entry is a line, stripped)
  #
  gaml_experiments = []
  in_experiment = False
  block_level = 0
  for line in lines:
    #
    # mark the experiment block
    #
    if line.startswith("experiment"):
      experiment = []
      in_experiment = True
    #
    # if in the experiment block then append the next line
    # if there's a brace, increase block level
    # if there's a closing brace, decrease block level
    # if the block level is zero then get out of
    # the experiment block
    #
    if in_experiment:
      # append experiment
      experiment.append(line)
      # increase block level
      if re.search("{", line) is not None:
        block_level = block_level + 1
      # decrease block level
      if re.search("}", line) is not None:
        block_level = block_level - 1
      # quit the experiment block
      if block_level == 0:
        in_experiment = False
        gaml_experiments.append(experiment)


  #
  # search for parameters definitions in gaml files
  #
  for line in lines:
    param = {}
    #
    # skip non-parameter lines
    #
    if not line.startswith("parameter"):
      continue
    #
    # read the description, description maybe encapsuled
    # in either '...' or "...", so there are 2 regexs
    # e.g. parameter "Number of cow: "
    #
    desc = re.search("parameter +'([^']+)'", line)
    if desc is None:
      desc = re.search('parameter +"([^"]+)"', line)
    if desc is None:
      desc = re.search('parameter +([^ ]+)', line)
    param['description'] = desc.group(1)
    #
    # variable name associated with the parameter
    # e.g. var: nb_init_aphids
    #
    variable = re.search("var: *([$a-zA-Z_][$a-zA-Z_0-9]*)", line).group(1)
    param['variable'] = variable
    param['datatype'] = variables[variable][0]
    param['default'] = variables[variable][1]
    #
    # read value restrictions if there's any
    # luckily, the syntax is the same as python list
    # so eval would be enough to convert it to python list
    # with one exception of True and true
    # e.g. among:  ["nn", "goc", "bien"]
    #
    among = re.search("among: *(\[.+])", line)
    if among is not None:
      among = among.group(1)
      among = among.replace("true", "True").replace("false", "False")
      param['among'] = eval(among)

    # add to list of params
    params.append(param)

  #
  # search for outputs in gaml files
  #
  for line in lines:
    if line.startswith("monitor"):
      output_type = "monitor"
    elif line.startswith("display"):
      output_type = "display"
    else:
      continue
    output = re.search("%s +'([^']+)'" % output_type, line)
    if output is None:
      output = re.search('%s +"([^"]+)"' % output_type, line)
    if output is None:
      output = re.search('%s +([^ ]+)' % output_type, line)
    outputs.append(output.group(1))

  return params, outputs, gaml_experiments

# vim: ts=2 sw=2 sts=2 smartindent expandtab
