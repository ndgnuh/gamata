from common import fbasename, outputsdir, inputsdir, csvsdir
from re import search
import os
import os.path as path

def parse_line(line):
	obj = search("\s*<Variable name='([A-Za-z0-9-_ ]+)' type='(\w+)'>([a-zA-Z0-9-_. ]+)</Variable>", line)
	if obj is None:
		return None, None, "Non-result line"
	else:
		key = obj.group(1)
		t = obj.group(2)
		val = obj.group(3)
		if t == "INT":
			val = int(val)
		elif t == "FLOAT":
			val = float(val)
		return key, val, None

def parse(f):
	result = {}
	io = open(f, "r")
	for line in io.readlines():
		key, val, err = parse_line(line)
		if type(err) is type(""):
			continue
		if key not in result:
			result[key] = []
		result[key].append(val)
	io.close()
	return result

def to_csv(fname, result, delim = ","):
	n = len(list(result.values())[0])
	rows = [[result[key][i] for key in result.keys()] for i in range(n)]
	rows = map(lambda row: str(row).strip("[]"), rows)
	rows = ("\n").join(rows)
	io = open(fname, "w")
	io.write(rows)
	io.close()
	return fname

def xml2csv(file1, file2):
	result = parse(file1)
	return to_csv(file2, result, ",")


def batch_xml2csv():
	outputdirs = []
	for d in os.listdir(outputsdir):
		outputdir = path.join(outputsdir, d)
		if path.isdir(outputdir) and d != csvsdir:
			outputdirs.append(outputdir)

	for outputdir in outputdirs:
		csvdir = path.join(outputdir, "csv")
		os.makedirs(csvdir, exist_ok = True)
		for itername in os.listdir(outputdir):
			iterdir = path.join(outputdir, itername)
			if not os.path.isdir(iterdir):
				continue
			for f in os.listdir(iterdir):
				if not ( f.startswith("simulation-output") and f.endswith(".xml") ):
					continue
				xmlfile = path.join(iterdir, f)
				csvfile = os.path.basename(xmlfile)
				csvfile = "%s-%s.csv" % (itername, csvfile)
				csvfile = path.join(csvdir, csvfile)
				xml2csv(xmlfile, csvfile)

if __name__ == "__main__":
	batch_xml2csv()
