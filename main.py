import sys
import os
import os.path as path
import subprocess
import hashlib
import shutil
sys.path.append(os.path.dirname(__file__))
import run
import result2csv
import config
from common import csvsdir, outputsdir
import log

# return a function that do proper padding base
# on the replication
#   pad = neatly_pad_nb_simulation(120)
#   pad(1) = 001
#   pad(12) = 012
#   pad(123) = 123
def neatly_pad_nb_simulation(x, char = '0'):
  npad = len(str(x))
  def pad(y, char = char):
    return str(y).rjust(npad, char)
  return pad


def xml_input_handler(f):
  #
  # run simulation
  #
  gama_headless = config.get_gama_headless()
  inputdir = config.get_inputdir()
  outputdir = config.get_outputdir()
  # log.info("Input dir: %s" % inputdir)
  # log.info("Output dir: %s" % outputdir)
  #
  # generating outputs name from xml content
  #
  xmlname = path.join(inputdir, f)
  output = path.basename(xmlname).replace(".xml", "")
  output = path.join(outputdir, output)
  #
  # create output dir
  #
  os.makedirs(output, exist_ok=True)
  #
  # check the simulation progress
  #
  replication = config.get_replication()
  pad = neatly_pad_nb_simulation(replication)
  shutil.copyfile(xmlname, path.join(output, "run.xml"))
  #
  # for each run sample
  #
  ok = True
  log.info("Output file: %s/*" % path.join(output))  
  for nrun in range(replication):
    nrun = pad(nrun + 1)
    output_nth = path.join(output, nrun)
    # try to create the output dir first
    try:
      os.makedirs(output_nth, exist_ok = False)
      log.info("** Replication #%s" % nrun)
##      if os.name == "nt":
##        gama_headless = r"{}".format(gama_headless)
##        xmlname = os.path.normpath("%r" % xmlname)
##        output_nth = os.path.normpath("%r" % output_nth)
##      log.info("CMD: %s" % ([gama_headless, xmlname, output_nth]))
      p = subprocess.run(
          [gama_headless, xmlname, output_nth],
          shell=True,
          stdout=subprocess.PIPE,
          stderr=subprocess.PIPE)
      # copy a file to indicate the experiment is done
      
      if p.returncode == 0:
        confirmname = path.join(output_nth, "ok")
        confirmfile = open(confirmname, "w")
        confirmfile.write(str(True))
        confirmfile.close()
      elif len(p.stderr) > 2:
        log.error("process failed with error %d %s" % (p.returncode, p.stderr))
        ok = False
    # if the output dir exists then quit
    except FileExistsError:
      log.warn("%s exists, skiping" % output_nth)
      continue
    # if there's key board interupt
    # delete all current files and quit
    # @TODO: nested deletion need testing
    except KeyboardInterrupt:
      for root, dirs, files in os.walk(output_nth):
        for file in files:
          os.remove(path.join(root, file))
        for dir in dirs:
          os.removedirs(path.join(root, dir))
      log.error("Keyboard Interupt detected, quit experimenting")
      ok = False
      break
    except Exception as e:
      log.error("Unknown error: %s" % str(e))
  return ok

def main(testing = False):
  inputdir = config.get_inputdir()
  outputdir = config.get_outputdir()
  if not path.isdir(inputdir):
    log.error("Inputs NOT Found")
    return
    #final_step = 100
  # ensure there's out directory
  os.makedirs(outputdir, exist_ok=True)
  ok = True
  for f in os.listdir(inputdir):
    if not f.lower().endswith(".xml"):
      continue
    log.info("Input file: %s" % str(f))
    if not ok:
      log.error("Something is wrong... Quitting")
    ok = ok and xml_input_handler(f)
  if ok:
    result2csv.batch_xml2csv()


if __name__ == "__main__":
  main()
  input("Press enter...")

# vim: sw=2:expandtab:ts=2
