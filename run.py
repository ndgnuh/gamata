from os import mkdir, path
import subprocess
import os
from os import name as osname
from time import time_ns
from common import inputsdir, outputsdir, fbasename

def gamaheadless():
   if osname == "posix":
      return path.join("..", "headless", "gama-headless.sh")
   else:
      return path.join("..", "headless", "gama-headless.cmd")


def run(xml, skip_exists = True, ntimes = 30):
   exitcode = 1
   basename = path.basename(xml).replace(".xml", "")
   outputdir = path.join(outputsdir, basename)
   gama = gamaheadless()
   shouldrun = True
   if path.isdir(outputdir):
      if skip_exists:
         print("Result %s exists, skipping" % outputdir)
         shouldlrun = False
      else:
         os.rename(outputdir, "%s-%s" %(outputdir, time_ns()))
   if shouldrun:
      os.makedirs(outputdir, exist_ok = True)
      for i in range(ntimes):
         count = i + 1
         ith_outputdir = path.join(outputdir, "%02d" % count)
         os.makedirs(ith_outputdir, exist_ok = True)
         exitcode = subprocess.run([gama, xml, ith_outputdir]).returncode
   return exitcode

def batch_run(skip_exists = None, ntimes = 30):
   xmlnames = [xml for xml in os.listdir(outputsdir) if xml.endswith(".xml")]
   for xmlname in xmlnames:
      xmlfile = path.join(outputsdir, xmlname)
      try:
         exitcode = run(xmlfile, skip_exists = skip_exists, ntimes = ntimes)
         if exitcode != 0:
            print("exitcode", exitcode)
            print("Lỗi %d: file %s" %(exitcode, xmlfile))
            break
      except KeyboardInterrupt:
         exitcode = 2
         print("Ngắt chương trình...")
         break
   return exitcode
